var gulp = require('gulp'),
    sass = require('gulp-sass'),
    watch = require('gulp-watch'),
    cleanCSS = require('gulp-clean-css');
    browserSync = require("browser-sync"),
    gutil = require('gulp-util'),
    reload = browserSync.reload;

var path = {
    build: { 
        html: 'build/',
        js: 'build/js/',
        css: 'build/css/',
        image: 'build/img/'

    },
    src: { 
        html: 'src/*.html', 
        js: 'src/js/**/*.js',
        sass: 'src/sass/**/*.scss',
        css: 'src/css/**/*.css',
        image: 'src/img/*'
    },
    watch: {
        html: 'src/**/*.html',
        js: 'src/js/**/*.js',
        sass: 'src/sass/*',
        css: 'src/css/**/*.css',
        image: 'src/img/*'
    },
    clean: './build'
};


function handleErrors() {
  var gutil = require('gulp-util');
  var args = Array.prototype.slice.call(arguments);

  gutil.log(gutil.colors.white.bgRed.underline.bold('Gulp error:'));
  gutil.log(gutil.colors.red(args));

  this.emit('end');
}
gulp.task('html',function(){
    gulp.src(path.src.html)
    .pipe(gulp.dest(path.build.html))
    .pipe(reload({stream: true}));
});

gulp.task('browser-sync', function() {
    browserSync.init({
        server: {
            baseDir: [path.build.html]
        }
    });
});
	


gulp.task('sass',function(){                
  gulp.src('./src/sass/main.scss')
    .pipe(sass({includePaths: ['./src/sass']}).on('error', sass.logError))	
    .pipe(sass().on('error', sass.logError))
    .pipe(cleanCSS({debug: true}, function(details) {
            console.log(details.name + ': ' + details.stats.originalSize);
            console.log(details.name + ': ' + details.stats.minifiedSize);
        }))
    .pipe(gulp.dest(path.build.css))
    .pipe(reload({stream: true}));

});



gulp.task('watch', function(){
    watch([path.watch.sass], function(event, cb) {
        gulp.start('sass');
    });
});


gulp.task('default', ["sass", "html", "browser-sync" ,"watch"]);	